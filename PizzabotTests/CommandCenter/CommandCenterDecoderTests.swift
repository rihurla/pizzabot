//
//  CommandCenterDecoder.swift
//  PizzabotTests
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import Pizzabot

class CommandCenterDecoderTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
   
  func testDecodeCommandNumberOfInstructions() {
    // Given
    let expectedInstructionsCount = 3
    let unitUnderTest:CommandCenterDecoder = CommandCenterDecoder()
    let commandString = "5x5 (1,3) (4,4)"
    // When
    do {
      let instructions = try unitUnderTest.decodeCommand(instruction: commandString)
      // Then
      XCTAssertEqual(instructions.count, expectedInstructionsCount)
    } catch  {
      XCTFail("Could not decode command: \(error.localizedDescription)")
    }

  }
  
  func testDecodeCommandCanvasSize() {
    // Given
    let expectedInstructionType = CommandCenterInstructionType.canvasSize
    let expectedInstructionValue = [5,5]
    let unitUnderTest:CommandCenterDecoder = CommandCenterDecoder()
    let commandString = "5x5 (1,3) (4,4)"
    // When
    do {
      let instructions = try unitUnderTest.decodeCommand(instruction: commandString)
      let canvasSize = instructions[0]
      // Then
      XCTAssertEqual(canvasSize.type, expectedInstructionType)
      XCTAssertEqual(canvasSize.value, expectedInstructionValue)
    } catch  {
      XCTFail("Could not decode command: \(error.localizedDescription)")
    }

  }
  
  func testDecodeCommandDeliveries() {
    // Given
    let expectedInstructionType = CommandCenterInstructionType.deliveryPosition
    var expectedInstructionValue = [1,3]
    let unitUnderTest:CommandCenterDecoder = CommandCenterDecoder()
    let commandString = "5x5 (1,3) (4,4)"
    // When
    do {
      let instructions = try unitUnderTest.decodeCommand(instruction: commandString)
      let firstDelivery = instructions[1]
      let secondDelivery = instructions[2]
      // Then
      XCTAssertEqual(firstDelivery.type, expectedInstructionType)
      XCTAssertEqual(firstDelivery.value, expectedInstructionValue)
      
      expectedInstructionValue = [4,4]
      
      XCTAssertEqual(secondDelivery.type, expectedInstructionType)
      XCTAssertEqual(secondDelivery.value, expectedInstructionValue)
    } catch {
      XCTFail("Could not decode command: \(error.localizedDescription)")
    }

  }
  
}
