//
//  RobotTests.swift
//  PizzabotTests
//
//  Created by Ricardo Hurla on 23/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import Pizzabot

class RobotTests: XCTestCase {
  
  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testIsInBounds() {
    // Given
    let expectedValue = false
    let canvas = Canvas(maxX: 5, maxY: 5)
    let unitUnderTest = Robot(canvas: canvas)
    let instruction = CommandCenterInstruction(type: .deliveryPosition, value: [1,3])
    // When
    let isInBounds = unitUnderTest.isOutOfBounds(instruction: instruction)
    // Then
    XCTAssertEqual(isInBounds, expectedValue)
  }
  
  func testIsOutOfBounds() {
    // Given
    let expectedValue = true
    let canvas = Canvas(maxX: 5, maxY: 5)
    let unitUnderTest = Robot(canvas: canvas)
    let instruction = CommandCenterInstruction(type: .deliveryPosition, value: [-1,6])
    // When
    let isInBounds = unitUnderTest.isOutOfBounds(instruction: instruction)
    // Then
    XCTAssertEqual(isInBounds, expectedValue)
  }
  
  func testRobotCommandDescription() {
    // Given
    let expectedValue = "N"
    let canvas = Canvas(maxX: 5, maxY: 5)
    let unitUnderTest = Robot(canvas: canvas)
    let instruction = RobotInstruction.directionNorth
    // When
    unitUnderTest.robotInstruction = instruction
    let instructionDescription = unitUnderTest.robotCommandDescription()
    // Then
    XCTAssertEqual(instructionDescription, expectedValue)
  }
  
  func testMoveOnXAxis() {
    // Given
    let expectedValue = "EEE"
    let canvas = Canvas(maxX: 5, maxY: 5)
    let unitUnderTest = Robot(canvas: canvas)
    let instruction = CommandCenterInstruction(type: .deliveryPosition, value: [3,0])
    // When
    let robotPath = unitUnderTest.moveOnXAxis(instruction: instruction)
    // Then
    XCTAssertEqual(robotPath, expectedValue)
  }
  
  func testMoveOnYAxis() {
    // Given
    let expectedValue = "NNN"
    let canvas = Canvas(maxX: 5, maxY: 5)
    let unitUnderTest = Robot(canvas: canvas)
    let instruction = CommandCenterInstruction(type: .deliveryPosition, value: [0,3])
    // When
    let robotPath = unitUnderTest.moveOnYAxis(instruction: instruction)
    // Then
    XCTAssertEqual(robotPath, expectedValue)
  }
  
  func testReceiveCommand() {
    // Given
    let expectedValue = "ENNND"
    let canvas = Canvas(maxX: 5, maxY: 5)
    let unitUnderTest = Robot(canvas: canvas)
    let instruction = CommandCenterInstruction(type: .deliveryPosition, value: [1,3])
    // When
    let robotPath = unitUnderTest.receiveCommand(instruction: instruction)
    // Then
    XCTAssertEqual(robotPath, expectedValue)
  }
  
  func testReceiveCommandWithMultipleInstructions() {
    // Given
    let expectedValue = "ENNNDEEEND"
    let canvas = Canvas(maxX: 5, maxY: 5)
    let unitUnderTest = Robot(canvas: canvas)
    let instruction1 = CommandCenterInstruction(type: .deliveryPosition, value: [1,3])
    let instruction2 = CommandCenterInstruction(type: .deliveryPosition, value: [4,4])
    // When
    let robotPath1 = unitUnderTest.receiveCommand(instruction: instruction1)
    let robotPath2 = unitUnderTest.receiveCommand(instruction: instruction2)
    let robotFullPath = robotPath1 + robotPath2
    // Then
    XCTAssertEqual(robotFullPath, expectedValue)
  }
  
}
