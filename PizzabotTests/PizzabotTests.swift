//
//  PizzabotTests.swift
//  PizzabotTests
//
//  Created by Ricardo Hurla on 23/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import Pizzabot

class PizzabotTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  /*
   🤖 This is a interface where the pizzabot can be tested.
  */
  func testPizzaBot() {
    // Given
    let expectedPath: String = "DENNNDEEENDSSDDWWWWSDEEENDWNDEESSD"
    let commandString: String = "5x5 (0,0) (1,3) (4,4) (4,2) (4,2) (0,1) (3,2) (2,3) (4,1)"
    let unitUnderTest = CommandCenter()
    do {
      // When
      let robotFullPath = try unitUnderTest.pizzabotDeliveryWith(instruction: commandString)
      // Then
      XCTAssertEqual(robotFullPath, expectedPath)
    } catch  {
      XCTFail("Could not delivery pizza command: \(error.localizedDescription)")
    }
  }
}
