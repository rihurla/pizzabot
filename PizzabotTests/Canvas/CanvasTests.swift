//
//  CanvasTests.swift
//  PizzabotTests
//
//  Created by Ricardo Hurla on 23/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import XCTest
@testable import Pizzabot

class CanvasTests: XCTestCase {

  override func setUp() {
    super.setUp()
  }
  
  override func tearDown() {
    super.tearDown()
  }

  func testCanvasSize() {
    // Given
    let expectedMaxX = 5
    let expectedMaxY = 5
    let unitUnderTest = Canvas(maxX: expectedMaxX, maxY: expectedMaxY)
    // When
    let canvasMaxX = unitUnderTest.maxX
    let canvasMaxY = unitUnderTest.maxY
    // Then
    XCTAssertEqual(canvasMaxX, expectedMaxX)
    XCTAssertEqual(canvasMaxY, expectedMaxY)
  }

}
