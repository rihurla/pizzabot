![Icon](https://bitbucket.org/rihurla/pizzabot/raw/fa247364c2565e54dcd6c7c9d0b3e7fcc0de5084/Readme_images/pizzabot_icon.png)
# Pizzabot
Pizzabot is an app that delivers pizza slices on the neighborhood grid, it tracks the delivery path and also displays an graph.
## Reviewer Instructions

### Prerequisites
Xcode and Simulator/iOS Device

### Installing

The app doesn't use any third party depencencies, so, there is no need for any installation other than the Xcode compenents.

## Running the tests

To run the tests on Xcode, select the Pizzabot target and device, and click on the Test option or press ⌘+U
### Tests

**PizzabotTests.swift** - This is the interface created for the solution. Given the command string, the robot will return the delivery path.  
**CanvasTests.swift** - Tests the canvas size variables.  
**RobotTests.swift** - Tests Robot bounds, description, movements and commands.  
**CommandCenterDecoderTests.swift** - Tests conversion of String commands to CommandCenterInstruction with separation of canvas and delivery instructions.  
**CommandCenterTests.swift** - Tests the canvas and robot functionality.  


### Delivery Graph
The delivery graph can switched on and off by using the following variable on the CommandCenter.swift.
``` 
var shouldDisplayDeliveryGraph: Bool
```

Graph example:
Given the variable command: **"5x5 (1,3) (4,4)"**  
The graph will be displayed like this:

![Graph](https://bitbucket.org/rihurla/pizzabot/raw/97eb54a79f9574ba912cdc7fa6f869a0a3f03772/Readme_images/delivery_graph.png)

## Author

* **Ricardo Hurla** - [Portfolio](https://rihurla.com)  -  [BitBucket](https://bitbucket.org/rihurla/)
