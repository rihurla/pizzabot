//
//  PizzabotViewModel.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

protocol PizzabotViewModelDelegate: class {
  func viewModel(_ viewModel: PizzabotViewModel, willCreateCanvasWith size: Array<Int>)
  func viewModel(_ viewModel: PizzabotViewModel, didFailPizzaDeliveryWith error: Error)
  func viewModel(_ viewModel: PizzabotViewModel, willCreateDeliveryWith robot: Robot)
}

class PizzabotViewModel: NSObject {
  
  let commandCenter = CommandCenter()
  weak var delegate: PizzabotViewModelDelegate?
  
  func startDeliveryWith(instruction: String) -> String? {
    commandCenter.canvasDelegate = self
    commandCenter.shouldDisplayDeliveryGraph = true
    
    do {
      let robotDeliveryPath = try commandCenter.pizzabotDeliveryWith(instruction: instruction)
      return robotDeliveryPath
    } catch {
      self.delegate?.viewModel(self, didFailPizzaDeliveryWith: error)
    }
    
    return nil
    
  }
  
}

extension PizzabotViewModel: CommandCenterCanvasDelegate {
  
  func willAddRobotDelivery(_ commandCenter: CommandCenter, robot: Robot) {
    self.delegate?.viewModel(self, willCreateDeliveryWith: robot)
  }
  
  func didSetCanvasSizeWith(_ commandCenter: CommandCenter, instruction: CommandCenterInstruction) {
    self.delegate?.viewModel(self, willCreateCanvasWith: instruction.value)
  }
  
}
