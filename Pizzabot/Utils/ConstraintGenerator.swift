//
//  ConstraintGenerator.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class ConstraintGenerator: NSObject {
  static func generateAttachmentConstraintsFor(subview: UIView, parentView: UIView) -> Array<NSLayoutConstraint> {
    
    let topConstraint = NSLayoutConstraint(item: subview,
                                           attribute: .top,
                                           relatedBy: .equal,
                                           toItem: parentView,
                                           attribute: .top,
                                           multiplier: 1.0,
                                           constant: 0)
    
    let bottomConstraint = NSLayoutConstraint(item: subview,
                                              attribute: .bottom,
                                              relatedBy: .equal,
                                              toItem: parentView,
                                              attribute: .bottom,
                                              multiplier: 1.0,
                                              constant: 0)
    
    let leadingConstraint = NSLayoutConstraint(item: subview,
                                               attribute: .leading,
                                               relatedBy: .equal,
                                               toItem: parentView,
                                               attribute: .leading,
                                               multiplier: 1.0,
                                               constant: 0)
    
    let trailingConstraint = NSLayoutConstraint(item: subview,
                                                attribute: .trailing,
                                                relatedBy: .equal,
                                                toItem: parentView,
                                                attribute: .trailing,
                                                multiplier: 1.0,
                                                constant: 0)
    
    return [topConstraint, bottomConstraint, leadingConstraint, trailingConstraint]
    
  }
}
