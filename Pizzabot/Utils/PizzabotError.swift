//
//  PizzabotError.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

struct PizzabotError: Error {
  
  let message: String
  
  init(_ message: String) {
    self.message = message
  }
  
  public var localizedDescription: String {
    return message
  }
  
}
