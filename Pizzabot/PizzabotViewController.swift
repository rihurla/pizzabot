//
//  ViewController.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class PizzabotViewController: UIViewController {
  
  var viewModel = PizzabotViewModel()
  var canvasView: CanvasView?
  @IBOutlet weak var canvasContainer: UIView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.viewModel.delegate = self
    self.canvasContainer.backgroundColor = UIColor.clear
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    let commandString = "5x5 (1,3) (4,4)"
    self.deliveryPizzaWith(command: commandString)
  }

  func configureCanvasWithSize(x: Int, y: Int) {
    let canvas = Canvas(maxX: x, maxY: y)
    let newCanvasView = CanvasView(frame: self.canvasContainer.frame, canvas: canvas)
    self.canvasContainer.addSubview(newCanvasView)
    canvasView = newCanvasView
    setCanvasConstraints()
  }
  
  func setCanvasConstraints() {
    guard let canvas = self.canvasView else {
      handleErrorWith(message: "Canvas failed creation")
      return
    }
    canvas.translatesAutoresizingMaskIntoConstraints = false
    let canvasConstraints = ConstraintGenerator.generateAttachmentConstraintsFor(subview: canvas,
                                                                                 parentView: self.canvasContainer)
    NSLayoutConstraint.activate(canvasConstraints)
  }
  
  func deliveryPizzaWith(command: String) {
    if let robotFullPath = viewModel.startDeliveryWith(instruction: command) {
      showDeliveryPath(robotPath: robotFullPath)
    }
  }
  
  func showDeliveryPath(robotPath: String) {
    let alertController = UIAlertController(title: "Delivery Path", message: robotPath, preferredStyle: .alert)
    let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in }
    alertController.addAction(action1)
    self.present(alertController, animated: true, completion: nil)
  }
  
  func handleErrorWith(message: String) {
    let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
    let action1 = UIAlertAction(title: "Ok", style: .default) { (action:UIAlertAction) in }
    alertController.addAction(action1)
    self.present(alertController, animated: true, completion: nil)
  }
  
}

extension PizzabotViewController: PizzabotViewModelDelegate {
  
  func viewModel(_ viewModel: PizzabotViewModel, willCreateDeliveryWith robot: Robot) {
    if let canvas = canvasView {
      canvas.addDeliveryFrom(robot: robot)
    }
   }
  
  func viewModel(_ viewModel: PizzabotViewModel, willCreateCanvasWith size: Array<Int>) {
    configureCanvasWithSize(x: size[0], y: size[1])
  }
  
  func viewModel(_ viewModel: PizzabotViewModel, didFailPizzaDeliveryWith error: Error) {
    handleErrorWith(message: error.localizedDescription)
  }
  
}
