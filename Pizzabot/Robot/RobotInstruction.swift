//
//  RobotDirection.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

enum RobotInstruction {
  case none
  case directionNorth
  case directionEast
  case directionSouth
  case directionWest
  case delivery
}
