//
//  RobotView.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class RobotDeliveryView: UIView {
  
  var pizzaImage: UIImage? = UIImage(named: "pizza_slice")
  var pizzaImageView: UIImageView
  var instruction: CommandCenterInstruction
  var margin: CGFloat
  var unitX: CGFloat
  var unitY: CGFloat
  
  init(frame: CGRect, instruction: CommandCenterInstruction, margin: CGFloat, unitX: CGFloat, unitY: CGFloat) {
    self.instruction = instruction
    self.margin = margin
    self.unitX = unitX
    self.unitY = unitY
    pizzaImageView = UIImageView(frame: frame)
    pizzaImageView.image = pizzaImage
    super.init(frame: frame)
    self.addSubview(pizzaImageView)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func didMoveToSuperview() {
    super.didMoveToSuperview()
    self.updatePizza()
  }
  
  func updatePizza() {
    guard let pizzaSuperView = self.superview else { return }
    let height = pizzaSuperView.frame.height
    let x = margin + (CGFloat(instruction.value[0]) * self.unitX) - (self.frame.width / 2)
    let y = height - (margin + (CGFloat(instruction.value[1]) * self.unitY) + (self.frame.height / 2))
    self.frame = CGRect(x: x, y: y, width: self.frame.width, height: self.frame.width)
    self.setNeedsDisplay()
  }
  
  func update(unitX: CGFloat, unitY: CGFloat) {
    self.unitX = unitX
    self.unitY = unitY
    self.updatePizza()
  }
  
}
