//
//  Robot.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class Robot: NSObject {
  
  let canvas: Canvas
  var robotInstruction: RobotInstruction
  var currentInstruction: CommandCenterInstruction
  
  init(canvas: Canvas) {
    self.canvas = canvas
    self.robotInstruction = RobotInstruction.none
    self.currentInstruction = CommandCenterInstruction(type: .deliveryPosition, value: [0,0])
    super.init()
  }
  
  func receiveCommand(instruction: CommandCenterInstruction) -> String {
    
    if instruction.type != .deliveryPosition ,
      instruction.value.count != 2,
      !isOutOfBounds(instruction: instruction) {
      return ""
    }
    
    let robotPathX = moveOnXAxis(instruction: instruction)
    let robotPathY = moveOnYAxis(instruction: instruction)
    currentInstruction = instruction
    robotInstruction = .delivery
    
    let robotFullPath = robotPathX + robotPathY + robotCommandDescription()
    return robotFullPath
  }
  
  func isOutOfBounds(instruction: CommandCenterInstruction) -> Bool {
    var isOut: Bool = false
    let newX = instruction.value[0] as Int
    let newY = instruction.value[1] as Int
    
    if newX < 0 || newX > canvas.maxX || newY < 0 || newY > canvas.maxY {
      isOut = true
    }
    return isOut
  }
  
  func moveOnXAxis(instruction: CommandCenterInstruction) -> String {
    let currentX = currentInstruction.value[0] as Int
    let newX = instruction.value[0] as Int
    var robotPath: String = ""
    
    if currentX < newX {
      robotInstruction = .directionEast
    } else if currentX > newX {
      robotInstruction = .directionWest
    } else {
      robotInstruction = .none
    }
    
    if robotInstruction != .none {
      var difference = newX - currentX
      if difference < 0 {
        difference = difference * -1
      }
      for _ in 1...difference {
        robotPath.append(robotCommandDescription())
      }
    }
    
    return robotPath
  }
  
  func moveOnYAxis(instruction: CommandCenterInstruction) -> String {
    let currentY = currentInstruction.value[1] as Int
    let newY = instruction.value[1] as Int
    var robotPath: String = ""
    
    if currentY < newY {
      robotInstruction = .directionNorth
    } else if currentY > newY {
      robotInstruction = .directionSouth
    } else {
      robotInstruction = .none
    }
    
    if robotInstruction != .none {
      var difference = newY - currentY
      if difference < 0 {
        difference = difference * -1
      }
      for _ in 1...difference {
        robotPath.append(robotCommandDescription())
      }
    }
    
    return robotPath
  }
  
  func robotCommandDescription() -> String {
    let commandString: String
    switch self.robotInstruction {
    case .none:
      commandString = ""
    case .directionNorth:
      commandString = "N"
    case .directionEast:
      commandString = "E"
    case .directionSouth:
      commandString = "S"
    case .directionWest:
      commandString = "W"
    case .delivery:
      commandString = "D"
    }
    return commandString
  }
}
