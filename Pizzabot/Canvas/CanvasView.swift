//
//  CanvasView.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import UIKit

class CanvasView: UIView {
  
  private let margin: CGFloat = 20.0
  private var unitX: CGFloat = 0.0
  private var unitY: CGFloat = 0.0
  private var width: CGFloat = 0.0
  private var height: CGFloat = 0.0
  
  private let canvas: Canvas
  private var viewBoundsObserver: NSKeyValueObservation?
  
  init(frame: CGRect, canvas: Canvas) {
    self.canvas = canvas
    super.init(frame: frame)
    self.backgroundColor = UIColor.clear
    viewBoundsObserver = self.layer.observe(\.bounds) { layer, _ in
      self.updateDeliveries()
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func draw(_ rect: CGRect) {
    
    self.updateData()
    
    var baseX: CGFloat = margin
    var baseY: CGFloat = margin
    
    let canvasColor = UIColor.black
    canvasColor.setFill()
    
    for _ in 0...canvas.maxX {
      UIRectFill(CGRect(x: baseX, y: baseY, width: 1, height: height))
      baseX += unitX
    }
    
    baseX = margin
    
    for _ in 0...canvas.maxY {
      UIRectFill(CGRect(x: baseX, y: baseY, width: width, height: 1))
      baseY += unitY
    }
    
  }
  
  func addDeliveryFrom(robot: Robot) {
    self.updateData()
    let pizzaSize: CGFloat = 20.0
    let pizzaRect = CGRect(x: 0, y: 0, width: pizzaSize, height: pizzaSize)
    let robotDeliveryView = RobotDeliveryView(frame: pizzaRect,
                                              instruction: robot.currentInstruction,
                                              margin:  margin,
                                              unitX: unitX,
                                              unitY: unitY)
    self.addSubview(robotDeliveryView)
  }
  
  func updateDeliveries() {
    self.updateData()
    for (_ , value) in self.subviews.enumerated() {
      if let deliveryView = value as? RobotDeliveryView {
        deliveryView.update(unitX: self.unitX, unitY: self.unitY)
      }
    }
  }
  
  func updateData() {
    width = frame.width - (margin * 2)
    height = frame.height - (margin * 2)
    unitX = width / CGFloat(canvas.maxX)
    unitY = height / CGFloat(canvas.maxY)
  }
  
}
