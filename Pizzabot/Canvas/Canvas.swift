//
//  Canvas.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

/*
 🤖 This class determines the canvas size that is going to be drawn on the screen.
 */
class Canvas: NSObject {
  
  let maxX: NSInteger
  let maxY: NSInteger
  
  init(maxX: NSInteger, maxY: NSInteger) {
    self.maxX = maxX
    self.maxY = maxY
    super.init()
  }
  
}
