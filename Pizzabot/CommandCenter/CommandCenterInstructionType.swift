//
//  CommandCenterInstructionType.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

enum CommandCenterInstructionType {
  case canvasSize
  case deliveryPosition
}
