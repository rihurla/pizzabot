//
//  CommandCenterDecoder.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

class CommandCenterDecoder: NSObject {

  func decodeCommand(instruction: String) throws -> [CommandCenterInstruction] {
    
    var instructions = [CommandCenterInstruction]()
    let arr = instruction.split(separator: " ")
    
    if(arr.count <= 1) {
      throw PizzabotError("Number of instructions insuficient")
    }
    
    for (index, each) in arr.enumerated() {
      
      if index == 0 {
        let canvasValues = each.split(separator: "x")
        if canvasValues.count == 2,
          let canvasFirstValue = canvasValues.first,
          let canvasSecondValue = canvasValues.last {
          
          let instruction1ValueInt = Int(canvasFirstValue) ?? 0
          let instruction2ValueInt = Int(canvasSecondValue) ?? 0
          let instruction = CommandCenterInstruction(type: .canvasSize,
                                                     value: [instruction1ValueInt,instruction2ValueInt])
          instructions.append(instruction)
        } else {
          throw PizzabotError("Invalid canvas instruction")
        }
        continue
      }
      
      var replacement = each.replacingOccurrences(of: "(", with: "")
      replacement = replacement.replacingOccurrences(of: ")", with: "")
      let valueSplit = replacement.split(separator: ",")
      if valueSplit.count == 2,
        let deliveryFirstValue = valueSplit.first,
        let deliverySecondValue = valueSplit.last {
        
        let instruction1ValueInt = Int(deliveryFirstValue) ?? 0
        let instruction2ValueInt = Int(deliverySecondValue) ?? 0
        let instruction = CommandCenterInstruction(type: .deliveryPosition,
                                                   value: [instruction1ValueInt,instruction2ValueInt])
        instructions.append(instruction)
      } else {
        throw PizzabotError("Invalid position instruction")
      }
    }

    return instructions
  }
  
}
