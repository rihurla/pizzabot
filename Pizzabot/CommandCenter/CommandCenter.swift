//
//  CommandCenter.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

import Foundation

protocol CommandCenterCanvasDelegate: class {
  func didSetCanvasSizeWith(_ commandCenter: CommandCenter, instruction: CommandCenterInstruction)
  func willAddRobotDelivery(_ commandCenter: CommandCenter, robot: Robot)
}

class CommandCenter: NSObject {
  
  weak var canvasDelegate: CommandCenterCanvasDelegate?
  let decoder = CommandCenterDecoder()
  var shouldDisplayDeliveryGraph = false
  
  func pizzabotDeliveryWith(instruction: String) throws -> String {
    
    let instructions: [CommandCenterInstruction] = try decoder.decodeCommand(instruction: instruction)
    var robotFullPath: String = ""
    
    var canvasInstructions = instructions.filter { $0.type == .canvasSize }
    if canvasInstructions.count > 1 {
      throw PizzabotError("Canvas has more than one size")
    }
    let canvasInstruction = canvasInstructions[0]
    
    if shouldDisplayDeliveryGraph {
      displayCanvasWith(instruction: canvasInstruction)
    }
 
    let canvasSizeInstruction = canvasInstruction.value
    let canvas = Canvas(maxX: canvasSizeInstruction[0], maxY:  canvasSizeInstruction[1])
    let robot = Robot(canvas: canvas)
    
    let deliveryInstructions = instructions.filter { $0.type == .deliveryPosition }
    
    for deliveryInstruction in deliveryInstructions {
      let robotPath = robot.receiveCommand(instruction: deliveryInstruction)
      robotFullPath.append(robotPath)
      if shouldDisplayDeliveryGraph {
        displayDeliveryViewWith(robot: robot)
      }
    }
    
    return robotFullPath
  }
  
  func displayCanvasWith(instruction: CommandCenterInstruction) {
    canvasDelegate?.didSetCanvasSizeWith(self, instruction: instruction)
  }
  
  func displayDeliveryViewWith(robot: Robot) {
    if robot.robotInstruction == .delivery {
      canvasDelegate?.willAddRobotDelivery(self, robot: robot)
    }
  }
  
}
