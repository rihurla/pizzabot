//
//  CommandCenterInstruction.swift
//  Pizzabot
//
//  Created by Ricardo Hurla on 21/11/2018.
//  Copyright © 2018 Ricardo Hurla. All rights reserved.
//

struct CommandCenterInstruction {
  var type: CommandCenterInstructionType
  var value: [Int]
}
